import React, {Component} from 'react';
import { MDBBtn } from "mdbreact";

import App from './App'
 
   

 
class List extends Component {
    
  constructor(props) {
    super(props)
    this.state = {
      hits: this.props.data,
    }
  }
 
  

    row = this.props.data.map(item => {
          
        //let url = `/images/small/${item.image}`;
        //let alt = ` foto van ${item.name}`;
        return (
             <tbody >
            <tr>
            
                <td ><img src={"data:image/jpg;base64," + item.foto} width="280"  /></td>
                <td style={{color:'white'}}>{item.naam}</td>
                <td style={{color:'white'}}>Aantal commits: {item.personen.length}</td>
                <td><MDBBtn rounded color="success" onClick={() => this.props.action('Detail', item)}>Detail</MDBBtn></td>
                
            </tr>
            </tbody>
        )
    })
    render() {
          
        return (
             
            <table>
               {this.row}
            </table>
            );
    }
}

export default List;