import React, {Component} from 'react';
import './App.css';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import { MDBBtn } from "mdbreact";
import { MDBInput } from "mdbreact";

import L from 'leaflet';
import 'leaflet-routing-machine';


class Detail extends Component {

latidude = (locatie)=>{
  var komma = locatie.indexOf(',')
  return locatie.substring(0, komma);
  
}
longitude = (locatie)=>{
  var komma = locatie.indexOf(',')
  return locatie.substring(komma +2);
  
}

  
     state = {
    lat: this.latidude(this.props.data.locatie),
    lng: this.longitude(this.props.data.locatie),
    zoom: 13,
  }

  
  img = this.props.data.personen.map(item=>{
    return(
      <span>
       <table>
       <tbody>
       <tr>
       <td><img src={"data:image/jpg;base64," + item.fotos} width="100"  /></td>
       <td>{item.voornaam}</td>
       </tr>
       </tbody>
       </table>
      </span>
      )
  })
  
 
  
    
 render() {
   
   
    const position = [this.state.lat, this.state.lng];
    return (
      <div>
      <h1>{this.props.data.naam}</h1>
      <Map center={position} zoom={this.state.zoom} id="mapid"  >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
        />
        <Marker position={position}>
          <Popup>
            <span>
            {this.img}
            </span>
          </Popup>
        </Marker>
        
      </Map>
       <p>{this.props.data.omschrijving}</p>
       
      <input id="foto" name="foto" type="file" required></input><br></br>
      <label name="naam">Naam:</label>
      <MDBInput  icon="user" id="naam" required/><br></br>
      <label name="commentaar">Comentaar:</label>
      <MDBInput id="commentaar" name="commentaar"  type="text" required /><br></br>
      <label name="ranking">Ranking:</label>
      <MDBInput id="ranking" name="ranking" type="number" min="1" max="10" required /><br></br>
      
        <MDBBtn rounded color="success" onClick={() => this.props.voegen(document.getElementById("naam").value,
        document.getElementById("commentaar").value, 
        document.getElementById("ranking").value,
        this.props.data.id, 
        document.getElementById("foto")
          )
        }>Voeg commentaar toe</MDBBtn>
      </div>
    );
  }
}

export default Detail; 