import React, { Component } from 'react';

const TableBody = props => {
    const rows = props.albumData.map((row, index) => {
        return (
            <tr key={index}>
                <td>{row.nummer}</td>
                <td>{row.titel}</td>
                <td>{row.kaft}</td>
                <td>{row.prijs}</td>
                <td><button onClick={() => props.removeAlbum(index)}>Delete</button></td>
           </tr>
        );
    });

    return <tbody>{rows}</tbody>;
}

class Table extends Component {
    render() {
        const {albumData, removeAlbum} = this.props;
        
        return (
            <table>
                <TableBody
                    albumData={albumData}
                    removeAlbum = {removeAlbum}
                />
            </table>
        );
    }
}

export default Table;