import React, { Component } from 'react';

 
import Table from './porps/Tabel'
 import Form from './porps/form'
import './index.css';
 
 class AppProps extends Component {
   state ={ albums:  []
   }
     removeAlbum = index => {
        const albums = this.state.albums.filter((album, i) => {
            return (i !== index);
        });
        this.setState({albums});
    }
    
    handleSubmit = album => {
    this.setState({albums: [...this.state.albums, album]});
}
    
     render() {
        // const { albums } = this.state;
        return (
           <div className="container">
              <Table
                albumData={this.state.albums}
                removeAlbum={this.removeAlbum}
              />
           <Form handleSubmit={this.handleSubmit}/>
          </div>
        )
    };
  
  
 
}
export default AppProps;
